# Importación datos ENUE
# 
# Author: Alicia Franco
# Date: 28/11/2021
# 

#### Paquetes ####
if(!require(pacman)) install.packages("pacman")
pacman::p_load(tidyverse, janitor, data.table, here)

#### Archivero #####
paths <- list(inp = here("import-clean/raw/"),
              out = here("import-clean/out/"))

#### Import ####
tables <- paste0("T", c("HOGAR", "SDEM", "MODULO"), ".csv")
var_to_select <- c("paren", "sexo", "edad", "ent", "niv",
                   "p2_6_1", "p2_6_2", "p2_6_3",
                   "p3_11", "p2_8_1", "p2_8_2",
                   "p5_6_1", "p5_6_4", "p5_6_5" ,
                   "p5_1", "p5_2", "p5_7", "p5_7a", "p5_8",
                   "p5_3_1","p5_3_2", "p5_3_3", "p5_3_4",
                   "p7_1_1", "p7_1_3", paste0("p7_2_", as.character(1:6)),
                   "fac_per", "fac_hog", "upm", "viv_sel", "hogar", "n_ren",
                   "upm_dis", "est_dis")
#### Functions ####
read_clean <- function(dirs){
  df <- fread(paste0(paths$inp, dirs)) %>% 
    clean_names() 
  
  if(dirs == "TMODULO.csv"){
    df <- df %>% 
      select(p6_3_1: p6_10a_7_4, any_of(var_to_select))
  }
  if(dirs != "TMODULO.csv"){
    df <- df %>% 
      select(any_of(var_to_select))
  }
  if(str_detect(dirs, "MODULO|SDEM") == T){
    df <- df %>% 
      mutate(id_per = paste(str_pad(upm, width = 7, pad = "0", side = "left"),
                            str_pad(viv_sel, width = 2, pad = "0", side = "left"),
                            hogar,
                            str_pad(n_ren, width = 2, pad = "0", side = "left"),
                            sep = "."
                            ) 
      ) 
  }
  df <- df %>% 
    mutate(id_viv = paste(str_pad(upm, width = 7, pad = "0", side = "left"),
                          str_pad(viv_sel, width = 2, pad = "0", side = "left"),
                          hogar,
                          sep = ".") 
           )
}


#### Build ####
enut <- map(tables, ~read_clean(dirs = .x)) %>% 
  reduce(left_join)  

#### Categorize ####
clean <- enut %>%
  mutate(
    mujer = as.integer(sexo == 2),
    hombre = as.integer(sexo == 1),
    licencia = as.integer(p5_6_1 == 1),
    estancia = as.integer(p5_6_4 == 1),
    pres_cuidado = as.integer(p5_6_5 == 1),
    trab_dom = as.integer(p2_6_1 == 1 | p2_6_2 == 1),
    cuid_enf = as.integer(p2_6_3 == 1),
    per_c_disc = as.integer(p3_11 == 1),
    empleo = as.integer(p5_1 == 1),
    p5_7 = case_when(p5_7 %in% 0 :99998 ~ as.integer(p5_7),
                     p5_7 == 99999 ~ NA_integer_),
    ingreso = case_when(
      p5_7a == 1 ~ as.numeric(p5_7 * 4),
      p5_7a == 2 ~ as.numeric(p5_7 * 2),
      p5_7a == 3 ~ as.numeric(p5_7),
      p5_7a == 4 ~ as.numeric(p5_7 / 12)
    ),
    participa = as.integer(p5_1 == 1 | p5_2 %in% 1:6 | p5_8 %in% 1:2)
  ) %>%
  group_by(id_viv) %>%
  mutate(
    jefa = ifelse(mujer == 1 & paren == 1, 1, NA_integer_),
    jefe = as.integer(hombre == 1, paren == 1, 1, NA_integer_),
    miem = paste(paren, collapse = "."),
    madre = as.integer(jefa == 1 & str_detect(miem, ".3")),
    padre = as.integer(jefe == 1 & str_detect(miem, ".3"))
  ) %>%
  fill(jefa, jefe) %>%
  mutate(parej_homo = as.integer(jefa == 1 &
                                   paren == 2 &
                                   mujer == 1 | jefe == 1 &
                                   paren == 2 & hombre == 1)) %>%
  ungroup() %>%
  mutate(across(
    c(p2_8_1, p2_8_2, p5_3_1:p5_3_4, p6_3_1:p6_10a_7_4),
    ~ replace_na(.x, 0)
  ))

hrs <- clean %>% 
  select(starts_with("id"),p2_8_1, p2_8_2, p5_3_1, p5_3_2, p5_3_3,p5_3_4) %>%
  mutate(hrs_remun = p5_3_1 + p5_3_2 + p5_3_3 + p5_3_4,
         hrs_trab_dom = p2_8_1 + p2_8_2)  %>% 
  select(starts_with("id"), starts_with("hrs"))

hrs_2 <- clean %>% 
  select(starts_with("id"),p6_3_1:p6_10a_7_4) %>% 
  select(starts_with("id"),contains("a")) %>% 
  mutate(across(ends_with("_2"), ~.x/60),
         across(ends_with("_4"), ~.x/60))

hrs_2$hrs_tnrh<-rowSums(hrs_2[ , 3:length(hrs_2)])

hrs_2 <- hrs_2 %>% select(starts_with("id"), hrs_tnrh)

clean_hr <- clean %>% 
  select(starts_with("id"), mujer:parej_homo, fac_per, fac_hog, sexo, ends_with("_dis")) %>% 
  left_join(., hrs) %>% 
  left_join(., hrs_2)


saveRDS(clean_hr, here("import-clean/out/clean_hr.rds"))
rm(hrs_2, hrs, clean)  
  
  
  



